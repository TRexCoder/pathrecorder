#!/usr/bin/env python3
#  -*- coding: utf-8 -*-

from kivy.app import App
from kivy.config import Config
from kivy.lang import Builder

from gui import PathRecorderGUI

WIDTH = 900  # 1100
HEIGHT = 600  # 700
Config.set('graphics', 'resizable', True)
Config.set('graphics', 'width', str(WIDTH))
Config.set('graphics', 'height', str(HEIGHT))

Builder.load_file('app.kv')


class PathRecorderApp(App):
    title = 'Path Recorder'

    def build(self):
        return PathRecorderGUI()


if __name__ == '__main__':
    # Window.clearcolor = (0.93,0.93,0.93,1)  # Background-Color
    app = PathRecorderApp()
    app.run()
