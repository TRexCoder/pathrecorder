# -*- coding: utf-8 -*-
import os
from random import randrange as random

import numpy as np
import pandas as pd


def unify(points, n_points):
    quantity_unified_points = unify_quantity(points, n_points)
    roi = calc_region_of_intrest(points)
    unified_roi = unify_region_of_intrest(roi)
    unified_points = unify_size(quantity_unified_points, unified_roi)
    return unified_points


def unify_quantity(points, n_points):
    if len(points) <= 0:
        return None
    insert_points = []
    deleted_point = []
    new_points = points[:]

    while len(new_points) != n_points:
        # berechne einen Distanzwert für jeden Punkt mit seinem Nachfolgern
        distanze_map = [[i + 1,
                         (new_points[i + 1][0] - new_points[i][0]) ** 2 +
                         (new_points[i + 1][1] - new_points[i][1]) ** 2]
                        for i in range(len(new_points) - 1)
                        if new_points[i][2] == 0]  # Nur Punkte die nicht beim Aufsetzen oder Absetzen erzeugt wurden

        # interpolation: hinzufügen eines Punktes zwischen zwei Punkten
        if len(new_points) < n_points:
            distanze_map.sort(key=lambda x: x[1], reverse=True)
            # Index der maximalen Distanz
            max_dist_index = distanze_map[0][0]
            # Berechnung dens Punkt in der mitte zweier Punkte
            # bsp.: p1(2,-4) p2(4,8) -> p12((4+2)/2,(8+(-4))/2)
            insert = [(new_points[max_dist_index - 1][0] + new_points[max_dist_index][0]) / 2,
                      (new_points[max_dist_index - 1][1] + new_points[max_dist_index][1]) / 2, 0]
            new_points.insert(max_dist_index, insert)
            insert_points.append(insert)

        # exclusion: löschen des Punktes zwischen drei Punkten
        elif len(new_points) > n_points:
            distanze_map.sort(key=lambda x: x[1], reverse=False)
            # Index der minimalen Distanz
            min_dist = distanze_map[0][1]
            indices_min_dist = [dist[0] for dist in distanze_map if dist[1] == min_dist]
            idx = indices_min_dist.pop(random(len(indices_min_dist)))
            # if 1 < idx < len(new_points) -1:
            deleted_point.append(new_points.pop(idx - 1))
            # else: deleted_point.append(new_points.pop(idx))
    return new_points, insert_points, deleted_point


def unify_size(points, region_of_intrest):
    unified_points = []
    x_start, y_start, x_end, y_end = region_of_intrest
    for x, y, z in points:
        px = float(x - x_start) / (x_end - x_start)
        py = float(y - y_start) / (y_end - y_start)
        pz = z
        unified_points.append([px, py, pz])
    return unified_points


def calc_region_of_intrest(points):
    sorted_points = sorted(points, key=lambda p: p[0])
    x_start = sorted_points[0][0]
    x_end = sorted_points[-1][0]
    sorted_points = sorted(points, key=lambda p: p[1])
    y_start = sorted_points[0][1]
    y_end = sorted_points[-1][1]
    return x_start, y_start, x_end, y_end


def unify_region_of_intrest(roi):
    x_start, y_start, x_end, y_end = roi
    x_size = x_end - x_start
    y_size = y_end - y_start

    if x_size == y_size:
        return roi

    x_center = x_size / 2 + x_start
    y_center = y_size / 2 + y_start

    if x_size > y_size:
        y_start = y_center - x_size / 2
        y_end = y_start + x_size

    else:
        x_start = x_center - y_size / 2
        x_end = x_start + y_size
    return x_start, y_start, x_end, y_end


def plot(points, filename, titel, lims=None):
    from matplotlib import pyplot as plt
    fig = plt.figure(1, figsize=(3, 3), dpi=80, facecolor='white', edgecolor='black')
    plt.clf()
    if not lims:
        plt.xlim([-0.025, 1.025])
        plt.ylim([-0.025, 1.025])
    else:
        plt.xlim(lims[0])
        plt.ylim(lims[1])
    plt.grid(True, which='both', axis='both')
    plt.title(titel)
    for i in range(len(points)):
        size = 20
        if points[i][2] > 0:
            color = 'red'
        elif points[i][2] < 0:
            color = 'green'
        else:
            color = 'blue'
            size = 5
        plt.scatter(points[i][0], points[i][1], s=size, color=color)
    plt.plot()
    fig.savefig(filename)


def to_matrix(unified_pointset, columns, rows=None, with_path_trace=False):
    if rows is None:
        rows = columns
    path = []
    for x, y, z in unified_pointset:
        new_x = int(round(x * columns)) - 1
        new_x = 0 if new_x < 0 else new_x
        new_y = int(round(y * rows)) - 1
        new_y = 0 if new_y < 0 else new_y
        path.append([new_x, new_y])

    i = 1
    matrix = np.zeros((columns, rows), np.uint32)
    for x, y in path:
        if with_path_trace:
            matrix[rows - 1 - y][x] = i
            i += 1
        else:
            matrix[rows - 1 - y][x] = 1
    return matrix


class DatasetFileManager:
    def __init__(self, file_unified_set):
        self.filename_unified_dataset = file_unified_set
        self.files_exist = os.path.exists(file_unified_set)

    def save_unified_set(self, points, label):
        self._create_unified_dataset(points)
        filename = self.filename_unified_dataset
        with open(filename, mode='a+') as file:
            file.write(str(self.last_id(filename)).zfill(7))  # ID
            file.write(',' + label)  # LABEL
            for point in points:
                file.write(',' + str(round(point[0], 4)))  # paths
                file.write(',' + str(round(point[1], 4)))  # Y
                file.write(',' + str(round(point[2], 4)))  # Z
            file.write('\n')

    def last_id(self, filename=None):
        if filename is None:
            file = self.filename_unified_dataset
        else:
            file = filename
        if not self.files_exist:
            return 1
        with open(file) as f:
            lid = sum(1 for _ in f)
        return lid

    @property
    def load_unified_set(self):
        filename = self.filename_unified_dataset
        if not self.files_exist:
            return None
        full_dataset = pd.read_csv(filename)
        full_dataset.drop(full_dataset.columns[[0]], axis=1, inplace=True)
        x = np.array(full_dataset.drop(full_dataset.columns[[0]], axis=1))
        y = np.array(full_dataset[full_dataset.columns[[0]]])
        y = [val for sublist in y for val in sublist]
        return x, y

    def _create_unified_dataset(self, points):
        if not os.path.exists(self.filename_unified_dataset):
            header = 'ID'
            header += ',LABEL'
            for i in range(len(points)):
                header += ',paths{0}'.format(i)
                header += ',Y{0}'.format(i)
                header += ',Z{0}'.format(i)
            with open(self.filename_unified_dataset, mode='w') as file:
                file.write(header + '\n')
        self.files_exist = True
