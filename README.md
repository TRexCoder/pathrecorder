# README #

##Requierments
- [Python 3](https://www.python.org/downloads/)
- [TFLearn](http://tflearn.org/installation/)
- [TensorFlow](http://tflearn.org/installation/)
- [NumPy](http://www.numpy.org/)
- [Pandas](http://pandas.pydata.org/)
- [Kivy](https://kivy.org)
- [Matplotlib](http://matplotlib.org/)

### What is this repository for? ###

The here offered code is only for demonstration to purpos acquire, preprocess, and record data.

Furthermore this should be a showcase for train a simple ann and perform predictions.


### Demo ###

[![Demo](https://img.youtube.com/vi/_Hs3LZu4Rww/0.jpg)](https://www.youtube.com/watch?v=_Hs3LZu4Rww)

### Who do I talk to? ###

* domktrex@gmail.com
