from kivy.graphics import Line, Color, Rectangle, Ellipse
from kivy.uix.widget import Widget


class TouchRecorderView(Widget):
    def __init__(self, **kwargs):
        self.did_touch_up = kwargs.pop('did_touch_up', None)
        super(TouchRecorderView, self).__init__(**kwargs)
        with self.canvas.before:
            Color(1, 1, 1, 1)  # set the colour to red
            self.rect = Rectangle(size=self.size, pos=(self.x + 5, self.y + 5))
        self.line = []
        x, y = self.rect.pos
        self.x_min = x
        self.x_max = self.rect.size[0] + x
        self.y_min = y
        self.y_max = self.rect.size[1] + y
        self.points = []
        self.is_in_focus = False
        self.bind(size=self.update_rect)

    def update_rect(self, *args):
        self.rect.pos = self.pos
        self.rect.size = (self.width, self.height)
        x, y = self.rect.pos
        self.x_min = x
        self.x_max = self.rect.size[0] + x
        self.y_min = y
        self.y_max = self.rect.size[1] + y
        self.clear()

    def on_touch_down(self, touch):
        # print(touch.uid)
        self.is_in_focus = True
        if self.is_valid_touch(touch):
            with self.canvas:
                self.add_point(touch, -1)
                Color(0, 0, 1, 1)
                self.line.append(Line(width=3, points=(touch.x, touch.y)))
                # self.draw_dot(touch.x, touch.y, size=10)

    def on_touch_move(self, touch):
        if self.is_valid_touch(touch):
            if len(self.line) > 0:
                self.add_point(touch, 0)
                self.line[-1].points += (touch.x, touch.y)
                # self.draw_dot(touch.x, touch.y, size=10)

    def on_touch_up(self, touch):
        if self.is_valid_touch(touch):
            self.canvas.clear()
            self.canvas.add(Color(1, 1, 1, 1))
            self.canvas.add(self.rect)
            self.add_point(touch, 1)
            # self.draw_dot(touch.x, touch.y, size=10)
            for line in self.line:
                self.canvas.add(Color(0, 0, 0, 1))
                self.canvas.add(line)
                # self.draw_dots(self.points, 10)
            if callable(self.did_touch_up):
                self.did_touch_up()
        self.is_in_focus = False

    def clear(self):
        self.canvas.clear()
        self.canvas.add(Color(1, 1, 1, 1))
        self.canvas.add(self.rect)
        self.points = []
        self.line = []

    def is_valid_touch(self, touch):
        res = self.x_min < touch.x < self.x_max and self.y_min < touch.y < self.y_max and self.is_in_focus
        return res

    def add_point(self, touch, z):
        self.points.append([int(touch.x), int(touch.y), z])

    def draw_dot(self, x, y, size=5, color=(0, 0, 0, 1), pos_offset=(0, 0)):
        r, g, b, alpha = color
        x_offset, y_offset = pos_offset
        with self.canvas:
            Color(r, g, b, alpha)
            Ellipse(size=(size, size), pos=(x - size / 2 + x_offset, y - size / 2 + y_offset))

    def draw_dots(self, points, size=5, color=(0, 0, 0, 1), pos_offset=(0, 0)):
        for p in points:
            self.draw_dot(p[0], p[1], size, color, pos_offset)

    def draw_rect(self, rect, stroke_size=2, color=(0, 1, 0, 1), margin=3):
        xs, ys, xe, ye = rect
        width = xe - xs + (stroke_size + margin) * 2
        height = ye - ys + (stroke_size + margin) * 2
        r, g, b, alpha = color
        with self.canvas:
            Color(r, g, b, alpha)
            Line(width=stroke_size, rectangle=(xs - stroke_size - margin, ys - stroke_size - margin, width, height))

    def redraw(self):
        self.canvas.add(Color(1, 1, 1, 1))  # set the colour to red
        self.canvas.add(Rectangle(size=self.size, pos=(self.x + 5, self.y + 5)))
        for line in self.line:
            self.canvas.add(Color(0, 0, 0, 1))
            self.canvas.add(line)
