#  -*- coding: utf-8 -*-

# import matplotlib
# matplotlib.use('module://kivy.garden.matplotlib.backend_kivy')
# from kivy.garden.matplotlib.backend_kivyagg import FigureCanvas
import subprocess
import time
from threading import Thread

import numpy as np
from kivy.clock import Clock
from kivy.uix.boxlayout import *
from kivy.uix.image import Image

import dnn
import path
from widgets import TouchRecorderView


class PathRecorderGUI(BoxLayout):
    def __init__(self, **kwargs):
        BoxLayout.__init__(self, **kwargs)
        self.is_debug = False
        self.data_writer = path.DatasetFileManager('data/records.csv')
        self.net = None
        self.is_on_training = False
        self.current_path = []
        self.paths = None
        self.labels = None
        self.one_hot_map = None
        self.train_data_loaded = False
        self.n_epoch = 400
        self.logfile = 'res/train.log'
        self.loading_count = 0
        self.console_cleaner = CleanOutput()
        self.ipoints = []
        self.dpoints = []
        self.__build()

    def __build(self):
        self.setup_net()
        parrent = self.ids.layout_canvas
        # Buttons
        btn_add = self.ids.btn_add_data
        btn_clear = self.ids.btn_clear_canvas
        btn_train = self.ids.btn_start_training
        btn_predict = self.ids.btn_predict
        # Bind callbackfunctions from Buttons
        btn_add.bind(on_press=self.on_press_add)
        btn_clear.bind(on_press=self.on_press_clear)
        btn_predict.bind(on_press=self.on_press_predict)
        btn_train.bind(on_press=self.on_press_train)
        # TODO: Überprüfe ob mit SDL2 das kivi-matplotlib-backend wieder funktioniert
        # self.plot_canvas = self.creat_plot_canvas()    # Das Kivy garden matplotlib backend geht nicht mehr
        # l.ids.plot.add_widget(self.plot_canvas)        # Das Kivy garden matplotlib backend geht nicht mehr
        # Plots
        self.plot_left_img = Image(source='res/plot_empty.png')  # Workaround -> Kivy garden matplotlib
        self.plot_rigth_img = Image(source='res/plot_empty.png')  # Workaround -> Kivy garden matplotlib
        self.ids.plot_left.add_widget(self.plot_left_img)  # Workaround -> Kivy garden matplotlib
        self.ids.plot_right.add_widget(self.plot_rigth_img)  # Workaround -> Kivy garden matplotlib
        # Labels
        self.lb_label_names = self.ids.recorded_labels_name
        self.lb_label_count = self.ids.recorded_labels_count
        self.lb_prediction = self.ids.lb_prediction
        self.lb_prediction_description = self.ids.lb_prediction_description
        self.lb_pred_acc_names = self.ids.predicted_labels_name
        self.lb_pred_acc_values = self.ids.predicted_labels_accuracy

        self.ti_train_console = self.ids.tb_training_output
        self.ti_data_label = self.ids.data_label
        self.ti_data_label.enabled = False

        self.hwWidget = TouchRecorderView(did_touch_up=self.__generate_data)
        # self.hwWidget.did_touch_up = self.generate_data
        parrent.add_widget(self.hwWidget)
        self.show_record_stats()

    def on_press_add(self, *args):
        print('add ', self.ti_data_label.text)
        self.hwWidget.redraw()
        self.hwWidget.export_to_png(
            'current_path/raw_imgs/{}_{}_raw.png'.format(str(self.data_writer.last_id()).zfill(7),
                                                         self.ti_data_label.text))
        if len(self.current_path) == 100 and self.ti_data_label.text != '' :

            self.data_writer.save_unified_set(self.current_path, self.ti_data_label.text)
            self.show_record_stats()
        self.on_press_clear()

    def on_press_clear(self, *args):
        print('clear')
        self.hwWidget.clear()
        self.current_path = []
        self.ipoints = []
        self.dpoints = []
        self.plot_left_img.source = 'res/plot_empty.png'
        self.plot_left_img.reload()
        self.plot_rigth_img.source = 'res/plot_empty.png'
        self.plot_rigth_img.reload()
        self.lb_pred_acc_values.text = ''
        self.lb_pred_acc_names.text = ''
        self.lb_prediction.text = '-'
        self.lb_prediction_description.text = ''

    def on_press_train(self, *args):
        print('train')
        if not self.is_on_training:
            self.is_on_training = True
            with open(self.logfile, 'w+') as f:
                f.write(' ')
            subprocess.Popen(['python3', 'trainer.py', '--epochs', str(self.n_epoch), '--logfile', self.logfile])
            self.console_cleaner.start()
            Clock.schedule_interval(self.update_train_console, 1.0 / 30.0)

    def on_press_predict(self, *args):
        print('predict')
        print(len(self.current_path))
        if len(self.current_path) == 100:
            data = np.reshape(self.current_path, [100, 3, 1])
            val = self.net.predict(data)
            index_hot = np.argmax(val)
            pred_hot = np.zeros(len(val))
            pred_hot[index_hot] = 1
            prediction = dnn.one_hot_to_label(pred_hot, self.one_hot_map)
            self.lb_prediction.text = prediction
            print(prediction)
            labels = sorted(self.one_hot_map)
            self.lb_pred_acc_names.text = ''
            self.lb_pred_acc_values.text = ''

            description = ''
            if 'ACW' in prediction:
                description += 'ACW: anti clockwise  '
            elif 'CW' in prediction:
                description += 'CW: clockwise  '
            if 'INOUT' in prediction:
                description += 'INOUT: from in to out  '
            elif 'OUTIN' in prediction:
                description += 'OUTIN: from out to in  '
            self.lb_prediction_description.text = description

            accuracies = []
            for i in range(len(val)):
                accuracies.append([labels[i], val[i]])
            accuracies.sort(key=lambda x: x[1], reverse=True)
            for label, acc in accuracies:
                self.lb_pred_acc_names.text += label + '\n'
                self.lb_pred_acc_values.text += str(acc).zfill(2) + '\n'
                print('{}:\t{}'.format(label, float(str(acc).zfill(2))))

    def __generate_data(self):
        org_points = self.hwWidget.points
        if len([point for point in org_points if point[2] == 0]) > 1:
            points, inserted_points, deleted_points = path.unify_quantity(org_points, 100)
            roi = path.calc_region_of_intrest(points)
            unified_roi = path.unify_region_of_intrest(roi)
            unified_points = path.unify_size(points, unified_roi)
            xs, ys, xe, ye = unified_roi
            f_plot_unified = 'res/plot_unified.png'
            f_plot_raw = 'res/plot_raw.png'
            plot_size = [[xs - 10, xe + 10], [ys - 10, ye + 10]]
            path.plot(unified_points, f_plot_unified, 'unified')
            path.plot(org_points, f_plot_raw, 'raw', plot_size)
            self.ipoints.extend(inserted_points)
            self.dpoints.extend(deleted_points)
            # self.current_path.extend(unified_points)
            self.current_path = unified_points
            if self.is_debug:
                self.hwWidget.draw_dots(self.dpoints, color=(1, 0, 0, 1))
                self.hwWidget.draw_dots(self.ipoints, color=(0, 1, 0, 1))
                self.hwWidget.draw_rect(roi)
                self.hwWidget.draw_rect(unified_roi, color=(1, 0, 0, 1), margin=9)
            self.plot_rigth_img.source = f_plot_unified
            self.plot_rigth_img.reload()
            self.plot_left_img.source = f_plot_raw
            self.plot_left_img.reload()

    def update_train_console(self, *args):
        if self.console_cleaner.output.strip() == '' and not self.train_data_loaded:
            self.ti_train_console.text = 'LOADING TRAINING DATA\n   '
            prog = int(self.loading_count / 5) % 4
            if prog == 0:
                self.ti_train_console.text += '|'
            elif prog == 1:
                self.ti_train_console.text += '/'
            elif prog == 2:
                self.ti_train_console.text += '-'
            elif prog == 3:
                self.ti_train_console.text += '\\'
        else:
            self.ti_train_console.text = self.console_cleaner.prev_out
            self.train_data_loaded = True

        self.loading_count += 1

        if not self.console_cleaner.is_alive():
            self.net.load_model()
            self.loading_count = 0
            self.train_data_loaded = False
            Clock.unschedule(self.update_train_console)

    def load_data(self):
        data = self.data_writer.load_unified_set
        if data is None:
            return
        self.paths, self.labels = data
        return data

    def show_record_stats(self):
        if self.load_data() is None:
            return
        sorted_keys, n_sorted_keys = dnn.count_labels(self.labels)
        label_names = ''
        label_counts = ''
        for label, count in sorted_keys:
            print('{}: {}'.format(label, count))
            label_names += label + ": \n"
            label_counts += " " + str(count) \
                            + "   (" + str(round(float(count * 100) / len(self.labels), 2)).zfill(3) + " %)\n"
        self.lb_label_names.text = label_names
        self.lb_label_count.text = label_counts
        for i in range(14 - len(sorted_keys)):
            self.lb_label_names.text += '\n'
            self.lb_label_count.text += '\n'

    def setup_net(self):
        if self.load_data():
            label_group = dnn.group_labels(self.labels)
            _, self.one_hot_map = dnn.gen_one_hot(self.labels, label_group)
            self.net = dnn.DeepNet([100, 3, 1], len(label_group))
        else:
            self.net = dnn.DeepNet([100, 3, 1], 1)


class CleanOutput(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.output = ''
        self.prev_out = ''

    def run(self):
        while True:
            # self.t_start = time.clock()
            self.output = ''
            with open('res/train.log', mode='r') as f:
                lines = f.readlines()
                output = []
                for line in lines:
                    output.append(str(line))

                for i in range(len(output)):
                    output[i] = output[i].replace('\x1b[2K', '')
                    output[i] = output[i].replace('\x1b[A\x1b[A', ' ')
                    output[i] = output[i].replace('\x1b[1m\x1b[32m', '')
                    output[i] = output[i].replace('\x1b[0m\x1b[0m', '')
                    output[i] = output[i].replace('[0m', '')
                    output[i] = output[i].replace('[', '')
                    i_start = output[i].find('-- iter')
                    i_end = output[i].find('Training Step:')
                    output[i] = output[i].replace(output[i][i_start:i_end], '')

                if len(output) > 50:
                    for i in range(len(output) - 50, len(output)):
                        self.output += output[i]
                else:
                    for i in range(len(output)):
                        self.output += output[i]

            # t_schedule = time.clock() - self.t_start
            # print(t_schedule)
            self.prev_out = self.output
            if self.output.find('ENDE') > 0:
                break
            time.sleep(0.05)
