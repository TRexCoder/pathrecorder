# -*- coding: utf-8 -*-
from os import path
from sklearn import model_selection as third_party
from tflearn import fully_connected, input_data, DNN
from tflearn import merge
from tflearn.layers.estimator import regression
from path import to_matrix

import time
import numpy as np


class DeepNet():
    def __init__(self, n_iputs, n_ouputs, model=None, m_file='res/ann.model'):
        self.n_iputs = n_iputs
        self.n_ouputs = n_ouputs
        self.one_hot_label_map = None
        self._init_model = self.setup_model(n_iputs, n_ouputs)
        self.model_file = m_file
        self.model = self._init_model if model is None else model
        self.load_model()
        self.is_on_train = False

    def setup_model(self, n_iputs, n_ouputs):
        #deepnet = input_data(shape=[None, 30, 30, 1], name='input')
        input = input_data(shape=[None, n_iputs[0], n_iputs[1], n_iputs[2]], name='input')
        deepnet = fully_connected(input, 82, activation='sigmoid')#200
        deepnet = fully_connected(deepnet, 30, activation='sigmoid')#50
        #deepnet = fully_connected(deepnet, n_ouputs, activation='softmax')
        #deepnet = merge([input,deepnet],mode='concat',axis=1)
        deepnet = fully_connected(deepnet, n_ouputs, activation='softmax')
        deepnet = regression(deepnet, optimizer='adam', learning_rate=0.004, loss='categorical_crossentropy',
                             name='targets')
        model = DNN(deepnet, tensorboard_verbose=2)
        return model


    def predict(self, input):
        #return np.round(self.model.predict(np.array(input).reshape([-1,100,3,1]))[0], 3)
        return np.round(self.model.predict(np.array([input]))[0], 3)

    def train(self, dataset, n_epoch=6000):
        #x_train, y_train, x_test, y_test, self.one_hot_label_map = train_test_split(dataset,preprocess_fnc=preprocess_data)
        x_train, y_train, x_test, y_test, self.one_hot_label_map = train_test_split(dataset)
        self.is_on_train = True
       # self.model = self._init_model
        self.model.fit({'input': x_train}, {'targets': y_train}, n_epoch=n_epoch, snapshot_epoch=False,
                       # validation_set=0.1,
                       show_metric=True,  # validation_set=({'input': test_x}, {'targets': test_y}),
                       run_id='dnn_train', batch_size=len(x_train))

        accuracy_train = self.model.evaluate(x_train, y_train)
        accuracy_test = self.model.evaluate(x_test, y_test)
        self.is_on_train = False
        self.save_model()
        return accuracy_train, accuracy_test, self.model

    def score(self, inputs, labels, batch_size=None):
        return self.model.evaluate(inputs, labels)

    def save_model(self):
        self.model.save(self.model_file)

    def load_model(self):
        if path.exists(self.model_file):
            try:
                self.model.load(self.model_file)
            except:
               self.model = self._init_model



def gen_one_hot(labels, lable_group):
    '''Erzeugt ONE-HOT-Werte (3=[0,0,1,0]) für Strings und Integer als Labels

    :param labels: array mit den labals die in ONE-HOT-Werte übersetzt werden sollen
    :type labels: list
    :param lable_group: die unterschiedlichen Label als dictionary
    :type lable_group: list

    :Beispiel:
        >>> my_label_group = ['a', 'b', 'c']
        >>> my_labels = ['c','a','b','b','a','c','c','a','b','b']
        >>> gen_one_hot(my_labels,my_label_group)
        (array([[0, 0, 1],
                [1, 0, 0],
                ...
                [0, 1, 0]])
    :rtype: np.array
    :return: Liste mit ONE-HOT-Werten
    '''

    lable_group = sorted(lable_group)
    one_hot_map = {}
    n = 0
    for label in lable_group:
        if one_hot_map.get(label) is None:
            one_hot = np.zeros(len(lable_group), np.uint8)
            one_hot[n] = 1
            one_hot_map[label] = one_hot
            n += 1
    one_hot_label = np.array([one_hot_map[label] for label in labels])
    return one_hot_label, one_hot_map


def one_hot_to_label(one_hot_value, one_hot_map):
    reversed_map = {}
    for key in one_hot_map:
        reversed_map[np.argmax(one_hot_map[key])] = key
    return reversed_map[np.argmax(one_hot_value)]


def preprocess_data(dataset):
    x, y = dataset
    paths = np.reshape(x, (len(x), 100, 3))
    matrices = np.array([to_matrix(path, 30, with_path_trace=True) for path in paths])
    x = matrices.reshape([-1, 30, 30, 1])
    return x, y


def train_test_split(dataset, test_size=0.2, one_hot=True, preprocess_fnc=None):
    x, y = preprocess_fnc(dataset) if preprocess_fnc is not None else dataset
    x_train, x_test, y_train, y_test = third_party.train_test_split(x, y, test_size=test_size)
    one_hot_map = None
    if one_hot:
        label_group = group_labels(y_test)
        y_train, one_hot_map = gen_one_hot(y_train, label_group)
        y_test, _ = gen_one_hot(y_test, label_group)
    return x_train.reshape([-1, 100,3, 1]), y_train, x_test.reshape([-1, 100,3, 1]), y_test, one_hot_map
    #return x_train, y_train, x_test, y_test, one_hot_map


def group_labels(labels):
    ''' extrahiert aus dem Labelset alle unterschiedlichen Labels und
        sortiert diese alphabetisch aufsteigend von A nach Z

    :param  labels(List): to compute
    :return: list of grouped labels sorted by name or number depending of the labels content
    '''
    group = {}
    for label in labels:
        if group.get(label) is None:
            group[label] = None
    return sorted(group)


def count_labels(labels):
    '''Gibt die Anzahl der Daten pro label aus. Und die Anzahl an label

    :param dataset: paths, labels
    :return: [[Label1, Anzahl Datensets von Label1],...,[LabelN, Anzahl Datensets von LabelN]], Anzahl an Label
    '''
    keys = {}
    keys[labels[0]] = 1
    for i in range(1, len(labels)):
        if keys.get(labels[i]):
            keys[labels[i]] += 1
        else:
            keys[labels[i]] = 1
    sorted_keys = sorted(sorted(keys.items(), key=lambda x: x[0]), key=lambda x: x[1], reverse=True)
    return sorted_keys, len(sorted_keys)
