#!/usr/bin/env python3
#  -*- coding: utf-8 -*-

import sys
import time
from optparse import OptionParser

from dnn import DeepNet, group_labels
from path import DatasetFileManager

parser = OptionParser()
parser.add_option("-l", "--logfile", dest="logfile", default=None,
                  help="write report to FILE", metavar="FILE", type="string")
parser.add_option("-e", "--epochs", dest="epochs", default=100,
                  help="number of epochs", type="int")

(options, args) = parser.parse_args()
logfile = options.logfile
n_epoch = options.epochs
print('----------------------------------------------------------')
print('LOADING DATA')
print('----------------------------------------------------------')
file_manager = DatasetFileManager('data/records.csv')
data_set = file_manager.load_unified_set
x, y = data_set
label_group = group_labels(y)
print('Number of paths: {}'.format(len(x)))
print('Number of path types: {}'.format(len(label_group)))
print('\nRecorded path types:')
for label in sorted(label_group):
    print('  - {}'.format(label))
net = DeepNet((100, 3, 1), len(label_group))
if logfile:
    sys.stdout = open(logfile, 'w+')
print('----------------------------------------------------------')
print('START TRAINING')
print('----------------------------------------------------------')
t_start = time.clock()
accuracy_train, accuracy_test, _ = net.train(data_set, n_epoch=n_epoch)
t_end = time.clock()
t_delta = t_end - t_start
print('\n\n----------------------------------------------------------')
print('RESULTS')
print('----------------------------------------------------------')
print('Delta t: {0} s'.format(str(round(t_delta,2)).zfill(3)))
print('Training Set Accuracy: {} %'.format(str(round(accuracy_train[0] * 100,2)).zfill(3)))
print('Test Set Accuracy: {} %'.format(str(round(accuracy_test[0] * 100,2)).zfill(3)))
print('\n\nENDE')
